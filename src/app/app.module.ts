import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireModule, AuthMethods, AuthProviders  } from 'angularfire2';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { ImageUploadModule } from 'angular2-image-upload';

import { AppComponent } from './app.component';
import { RoomsListComponent } from './rooms-list/rooms-list.component';
import { NavComponent } from './nav/nav.component';
import { AF } from "./providers/af";
import { SharedService } from "./providers/shared-service";
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { SplashComponent } from './splash/splash.component';
import { UsersSearchComponent } from './users-search/users-search.component';
import { ChatWindowComponent } from './chat-window/chat-window.component';
import { TextPipe } from './providers/text-pipe';
import { SpinnerComponent } from './spinner/spinner.component';
import { MessageComponent } from './message/message.component';

export const firebaseConfig = {
    apiKey: "AIzaSyB7KBhJ390ykDLu42JN9u0Yg_8MrGaCmBw",
    authDomain: "angular2-project-7e0e0.firebaseapp.com",
    databaseURL: "https://angular2-project-7e0e0.firebaseio.com",
    storageBucket: "angular2-project-7e0e0.appspot.com",
    messagingSenderId: "622199537342"
}

const appRoutes: Routes = [
  { path: 'rooms', component: RoomsListComponent },
  { path: '', component: HomePageComponent },
  { path: 'splash', component: SplashComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegistrationPageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    RoomsListComponent,
    NavComponent,
    LoginPageComponent,
    HomePageComponent,
    RegistrationPageComponent,
    SplashComponent,
    UsersSearchComponent,
    ChatWindowComponent,
    TextPipe,
    SpinnerComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig,{
      provider: AuthProviders.Google,
      method: AuthMethods.Popup
    }),
    ToasterModule,
    ImageUploadModule.forRoot()
  ],
  providers: [AF, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
