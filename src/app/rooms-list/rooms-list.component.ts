import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.css']
})
export class RoomsListComponent implements OnInit {

  @Input() rooms: Room[];
  @Output() onRoomSelected: EventEmitter<Room> = new EventEmitter<Room>();
  @Output() onCreateNewRoom: EventEmitter<string> = new EventEmitter<string>();

  private toasterService: ToasterService;

  newRoomName: string;

  selectedItem: Room;

  constructor(toasterService: ToasterService) {
    this.toasterService = toasterService;
  }

  ngOnInit() {
  }

  setCurrentRoom(room){
    this.selectedItem = room;
    this.onRoomSelected.emit(room);
  }

  createRoom(){
    if(this.newRoomName == null || this.newRoomName == ""){
      this.popToast();
    }else{
      this.onCreateNewRoom.emit(this.newRoomName);
      this.newRoomName = null;
    }
  }

  popToast() {
      this.toasterService.pop('info', '', "Don't forget to name your room");
  }

  public toasterconfig : ToasterConfig = 
        new ToasterConfig({
            showCloseButton: true, 
            tapToDismiss: true, 
            timeout: 2000
        });

}

export class Room{
  name: string;
  admin: string;
  uid: string;
}
