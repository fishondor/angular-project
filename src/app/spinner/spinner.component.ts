import { Component, OnInit } from '@angular/core';

/**
 * Based on https://github.com/lukehaas/css-loaders
 */

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
