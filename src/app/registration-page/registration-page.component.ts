import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster';

import {AF} from "./../providers/af";
import {SharedService} from "../providers/shared-service";

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})

export class RegistrationPageComponent{

  public hasBaseDropZoneOver:boolean = false;

  public error: any;

  private photoURL = null;

  private showSpinner: boolean = false;

  constructor(private afService: AF, private router: Router, private _sharedService: SharedService, private toasterService: ToasterService) { }
	//registers the user and logs them in
  register(event, name, email, password) {
    event.preventDefault();
    this.showSpinner = true;
    this.afService.registerUser(name, email, password).then((user) => {
      if(this.photoURL != null){
        this.afService.uploadImage(email, this.photoURL).then(
          (response: string) =>{
            this.afService.saveUserInfoFromForm(user.auth.uid, name, email, response).then(
              response2 => {
                user.auth.updateProfile({
                        displayName: name,
                        photoURL: response
                      });
                this.afService.displayName = name;
                this.afService.email = email;
                this.afService.uid = user.auth.uid;
                this.afService.photoURL = response;
                this._sharedService.setDisplayName(name);
                this._sharedService.setPhotoUrl(response);
                this._sharedService.setisLoggedIn(true);
                this._sharedService.emitUserDataChange(true);
                this.router.navigate[''];
                this.showSpinner = false;
              }
            );
          }
        ).catch(error => {
          console.log("error: " + error);
          this.showSpinner = false;
        })
      }else{
        this.afService.saveUserInfoFromForm(user.auth.uid, name, email, null).then(
              response2 => {
                user.auth.updateProfile({
                        displayName: name,
                        photoURL: this.photoURL
                      });
                this.afService.displayName = name;
                this.afService.email = email;
                this.afService.uid = user.auth.uid;
                this.afService.photoURL = this.photoURL;
                this._sharedService.setDisplayName(name);
                this._sharedService.setPhotoUrl(this.photoURL);
                this._sharedService.setisLoggedIn(true);
                this._sharedService.emitUserDataChange(true);
                this.router.navigate[''];
                this.showSpinner = false;
              }
            );
      }
    }).catch((error) => {
      this.error = error;
      console.log(this.error);
      this.showSpinner = false;
    });
  }

  imageUploaded(event){
    if(event.file.size > 500000){
      this.popToast();
    }else{
      this.photoURL = event.file;
    }
  }

  imageRemoved(event){
    this.photoURL = null;
  }

  popToast() {
      this.toasterService.pop('info', '', "Sorry. We accept files up to 500Kb, this image will not be uploaded. Please replace it.");
  }

  public toasterconfig : ToasterConfig = 
        new ToasterConfig({
            showCloseButton: true, 
            tapToDismiss: true, 
            timeout: 2000
        });

}
