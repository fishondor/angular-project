import {Component, OnInit, OnDestroy} from '@angular/core';
import {FirebaseListObservable} from "angularfire2";
import 'rxjs/Rx';

import {AF} from "../providers/af";
import {SharedService} from "../providers/shared-service";

import {RoomsListComponent, Room} from "../rooms-list/rooms-list.component";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

export class HomePageComponent implements OnInit, OnDestroy {

  public messages: FirebaseListObservable<any>;

  rooms: Room[] = [];
  currentRoom;
  currentRoomId: string;
  currentRoomAdminId: string;
  currentUserId: string;
  
  resultsInGroup: User[] = [];
  resultsNotInGroup: User[] = [];
  allUsers: User[] = [];

  roomsSubscription;
  usersSubscription;
  userRoomsSu
  

  constructor(public afService: AF, private _sharedService: SharedService) {
    this.messages = this.afService.messages;
    this.roomsSubscription = this.afService.getRoomsForUser().subscribe(items => { 
        items.map(
          item => {
            item.details = {};
            let roomSubscription = this.afService.getRoomById(item.$key)
              .subscribe( data => {
                let r = new Room;
                r.admin = data.admin;
                r.name = data.name;
                r.uid = item.$key;
                this.rooms.push(r);
                roomSubscription.unsubscribe();
              });
          }
        );  
    });
    this.currentUserId = this.afService.getCurrentuserId();
    this.setCurrentRoom(null);
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.roomsSubscription.unsubscribe();
    this.usersSubscription.unsubscribe();
  }

  setUsers(){
    this.resultsInGroup = [];
    this.resultsNotInGroup = [];
    this.usersSubscription = this.afService.getAllUsers().subscribe(
      users => {
        users.map(
          user => {
            let u = new User;
            u.name = user.displayName;
            u.uid = user.$key;
            if(user.rooms && user.rooms[this.currentRoomId]){
              this.resultsInGroup.push(u);
            }else{
              this.resultsNotInGroup.push(u);
            }
          }
        )
      }
    );
  }

  sendMessage(message){
    this.afService.sendMessage(message);
  }

  createRoom(newRoomName){
    this.rooms = [];
    this.afService.createRoom(newRoomName)
      .then((room) => {
        this.afService.addRoomToUser(this.afService.getCurrentuserId(), room.key);
      });
  }

  setCurrentRoom(room){
    this.currentRoom = room;
    this.afService.setRoom(room ? room.uid : room);
    this.messages = this.afService.messages;
    this.currentRoomId = room ? room.uid : room;
    this.currentRoomAdminId = room ? room.admin : room;
    this.setUsers();
    this._sharedService.emitChange(this.currentRoom ? this.currentRoom.name : "Main");
  }

  getCurrentRoom(){
    return this.currentRoom;
  }

  removeUserFromRoom(user){
    this.afService.removeRoomFromUser(user.uid, this.currentRoomId).then(() => {
      this.setUsers();
    });
  }

  addUserToRoom(user){
    this.afService.addRoomToUser(user.uid, this.currentRoomId).then(() => {
      this.setUsers();
    });
  }

  getParticipantsTitle(){
    return this.currentRoomAdminId == this.currentUserId ?
      'Press user to remove from this room' :
      'Active users' ;
  }
}

export class User{
  name: string;
  uid: string;
}