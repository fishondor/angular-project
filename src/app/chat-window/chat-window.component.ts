import { Component, OnInit, Input, Output, EventEmitter, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import {FirebaseListObservable} from "angularfire2";
import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css']
})
export class ChatWindowComponent implements OnInit, AfterViewChecked {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  @Input() messages: FirebaseListObservable<any>;
  @Input() currentUserId: string;
  @Output() onMessage: EventEmitter<string> = new EventEmitter<string>();

  private newMessage: string;

  constructor(private toasterService: ToasterService) { }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { console.log('Could not scroll to bottom - report') }
  }

  sendMessage(){
    if(!this.newMessage || this.newMessage == ""){
      this.popToast()
    }else{
      this.onMessage.emit(this.newMessage);
      this.newMessage = "";
    }
  }

  popToast() {
      this.toasterService.pop('info', '', "Don't leave an empty message. Say something!!");
  }

  public toasterconfig : ToasterConfig = 
        new ToasterConfig({
            showCloseButton: true, 
            tapToDismiss: true, 
            timeout: 2000
        });

}
