import { Component, OnInit } from '@angular/core';

/**
 * Based on https://github.com/lukehaas/css-loaders
 */

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.css']
})
export class SplashComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
