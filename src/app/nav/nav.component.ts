import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {SharedService} from "../providers/shared-service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  private isLoggedIn: boolean;
  private userName: string;
  private avatar: string;

  @Output() onLogout: EventEmitter<any> = new EventEmitter<any>();

  private roomTitle: string;

  constructor(private _sharedService: SharedService) {
    _sharedService.changeEmitted$.subscribe(
        text => {
            this.roomTitle = text;
        });
    _sharedService.userDataChangeEmitted$.subscribe(
      () => {
        this.userName = _sharedService.userDisplayName;
        this.avatar = _sharedService.userPhotoUrl;
        this.isLoggedIn = _sharedService.isLoggedIn;
      }
    )
    this.userName = _sharedService.userDisplayName;
    this.avatar = _sharedService.userPhotoUrl;
    this.isLoggedIn = _sharedService.isLoggedIn;
  }

  logOut(){
    this.onLogout.emit();
  }

  ngOnInit() {
  }

}
