import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {AutoCompleteModule} from 'primeng/primeng';
import {AF} from "../providers/af";
import {FirebaseListObservable} from "angularfire2";
import 'rxjs/Rx';

import {User} from '../entities/user';

@Component({
  selector: 'app-users-search',
  templateUrl: './users-search.component.html',
  styleUrls: ['./users-search.component.css']
})

export class UsersSearchComponent implements OnInit {

  @Input() users: User[];
  @Input() currentUserId: string;
  @Input() roomAdminId: string;
  @Output() itemSelected: EventEmitter<User> = new EventEmitter<User>();
  
  constructor() {
  }

  ngOnInit() {
  }

  onItemSelected(item){
    this.itemSelected.emit(item);
  }

}
