import { Component } from '@angular/core';
import {AF} from "../providers/af";
import {Router} from "@angular/router";

import {SharedService} from "../providers/shared-service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent {

  public error: any;

  constructor(public afService: AF, private router: Router, private _sharedService: SharedService) {}

  loginWithGoogle() {
    this.afService.loginWithGoogle().then((data) => {
      this.afService.saveLocalUser(data.auth.uid, data.auth.displayName, data.auth.email, data.auth.photoURL);
      this.afService.validateUserNotExists(data.auth.uid).then(
        (response) => {
          console.log("response: " + response);
            this.afService.saveUserInfoFromForm(data.auth.uid, data.auth.displayName, data.auth.email, data.auth.photoURL).then(
                (response2) => {
                  this._sharedService.setDisplayName(data.auth.displayName);
                  this._sharedService.setPhotoUrl(data.auth.photoURL);
                  this._sharedService.setisLoggedIn(true);
                  this._sharedService.emitUserDataChange(true);
                  this.router.navigate(['']);
                }
              );
        }
      ).catch(
        error => {
          console.log("Entry exists, redirecting");
          this.router.navigate(['']);
        }
      );
    })
  }

  loginWithEmail(event, email, password){
    event.preventDefault();
    this.afService.loginWithEmail(email, password).then((data) => {
      this.afService.saveLocalUser(data.auth.uid, data.auth.displayName, data.auth.email, data.auth.photoURL);
                this._sharedService.setDisplayName(data.auth.displayName);
                this._sharedService.setPhotoUrl(data.auth.photoURL);
                this._sharedService.setisLoggedIn(true);
                this._sharedService.emitUserDataChange(true);
      this.router.navigate(['']);
    })
      .catch((error: any) => {
        if (error) {
          this.error = error;
          console.log(this.error);
        }
      });
  }
}
