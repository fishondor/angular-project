import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class SharedService {
    // Observable string sources
    private emitChangeSource = new Subject<any>();
    // Observable string streams
    changeEmitted$ = this.emitChangeSource.asObservable();
    // Service message commands
    emitChange(change: any) {
        this.emitChangeSource.next(change);
    }

    private emitChangeUserData = new Subject<any>();
    // Observable string streams
    userDataChangeEmitted$ = this.emitChangeUserData.asObservable();
    // Service message commands
    emitUserDataChange(change: any) {
        this.emitChangeUserData.next(change);
    }

    userPhotoUrl: string = null;
    userDisplayName: string = null;
    isLoggedIn: boolean = false;

    setPhotoUrl(photoUrl){
        this.userPhotoUrl = photoUrl;
    }

    setDisplayName(displayName){
        this.userDisplayName = displayName;
    }

    setisLoggedIn(isLoggedIn){
        this.isLoggedIn = isLoggedIn;
    }

}