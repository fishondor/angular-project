import {Injectable} from "@angular/core";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseApp} from 'angularfire2';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
import { Router } from "@angular/router";

import {SharedService} from "../providers/shared-service";

@Injectable()
export class AF {

  public messages: FirebaseListObservable<any>;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  public uid: string;
  public photoURL: string;

  constructor(public af: AngularFire, private _sharedService: SharedService, private router: Router) {
  }

  saveLocalUser(uid, name, email, photoURL){
    this.uid = uid;
    this.displayName = name;
    this.email = email;
    this.photoURL = photoURL;
  }

  /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithGoogle() {
    return this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
  }

  /**
   * Logs out the current user
   */
  logout() {
    this.messages = null;
    this.router.navigate(['login']);
    return this.af.auth.logout().then(
      () => {
        this._sharedService.setDisplayName(null);
        this._sharedService.setPhotoUrl(null);
        this._sharedService.setisLoggedIn(false);
        this._sharedService.emitUserDataChange(true);
      }
    );
  }

  /**
   * Creates a chat room
   */
  createRoom(name){
    return this.af.database.list('rooms').push({
      name: name,
      admin: this.uid
    });
  }

  /**
   * Adds room to user
   */
  addRoomToUser(userId, roomId){
    return this.af.database.object('registeredUsers/' + userId + '/rooms/' + roomId).set(true);
  }

  removeRoomFromUser(userId, roomId){
    return this.af.database.object('registeredUsers/' + userId + '/rooms/' + roomId).remove();
  }

  getCurrentuserId(){
    return this.uid;
  }

  /**
   * Gets rooms of current user
   * @returns {firebase.Observable<void>}
   */
  getRoomsForUser(){
    return this.af.database.list('/registeredUsers/'+this.uid+'/rooms');
  }

  /**
   * Gets all rooms
   * @returns {firebase.Observable<void>}
   */
  getAllRooms(){
    return this.af.database.list('rooms');
  }

  /**
   * get single room by id
   */
  getRoomById(roomId){
    return this.af.database.object('rooms/' + roomId);
  }

  /**
   * @returns {FirebaseListObservable}
   */
  getAllUsers(){
    return this.af.database.list('registeredUsers');
  }

  /**
   * sets messages observable on specific room
   */
  setRoom(roomKey){
    if(roomKey)
      this.messages = this.af.database.list('rooms/' + roomKey + '/messages');
    else
      this.messages = this.af.database.list('messages');
  }

  /**
   * Saves a message to the Firebase Realtime Database
   * @param text
   */
  sendMessage(text) {
    var message = {
      message: text,
      displayName: this.displayName,
      email: this.email,
      avatar: this.photoURL,
      user: this.uid,
      timestamp: Date.now()
    };
    this.messages.push(message);
  }

  /**
   * checks if this user is me
   */
  isMe(id){
    return id == this.uid || id == this.email;
  }

  /**
   * Calls the AngularFire2 service to register a new user
   * @param model
   * @returns {firebase.Promise<void>}
   */
  registerUser(name, email, password) {
    return this.af.auth.createUser({
      email: email,
      password: password
    });
  }

  /**
   * Saves information to display to screen when user is logged in
   * @param uid
   * @param model
   * @returns {firebase.Promise<void>}
   */
  saveUserInfoFromForm(uid, name, email, photoURL) {
      return this.af.database.object('registeredUsers/' + uid).set({
        name: name,
        email: email,
        displayName: name,
        photoURL: photoURL
      });
  }

  uploadImage(name, data) {
    let promise = new Promise((res,rej) => {
        let fileName = name + ".jpg";
        let uploadTask = firebase.storage().ref(`/avatars/${fileName}`).put(data);
        uploadTask.on('state_changed', function(snapshot) {
        }, function(error) {
            console.log("error: " + error);
            rej(error);
        }, function() {
            var downloadURL = uploadTask.snapshot.downloadURL;
            res(downloadURL);
        });
    });
    return promise;
  }

  validateUserNotExists(uid){
    return new Promise((resolve, reject) => {this.af.database.object('registeredUsers/' + uid).subscribe(data => {
      if(data.$value === null) {
        console.log('User does not exist');
        resolve(true);
      } else {
        console.log('User does exist');
        reject('username is taken');
      }
    })});
  }
  
   /**
   * Logs the user in using their Email/Password combo
   * @param email
   * @param password
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithEmail(email, password) {
    return this.af.auth.login({
        email: email,
        password: password,
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      });
  }
}