import {Pipe, PipeTransform} from '@angular/core';

// Tell Angular2 we're creating a Pipe with TypeScript decorators
@Pipe({
  name: 'TextPipe'
})
export class TextPipe implements PipeTransform{

  // Transform is the new "return function(value, args)" in Angular 1.x
  transform(value, args) {
    // ES6 array destructuring
    let searchText = args;
    if(searchText && searchText != "")
      return value.filter(user => {
        return  user.name.includes(searchText);
      });
    else 
      return value;
  }

}