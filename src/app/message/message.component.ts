import { Component, OnInit, Input } from '@angular/core';

import {AF} from "../providers/af";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input() message;

  constructor(public afService: AF) { }

  ngOnInit() {
    this.message.avatar =
      this.message.avatar ? 
      this.message.avatar : 
      "./assets/avatar_ninja.png";
  }

  isMe(uidOrMail){
    return this.afService.isMe(uidOrMail);
  }

}
