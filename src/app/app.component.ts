import { Component } from '@angular/core';
import { AngularFire, AuthProviders } from 'angularfire2';
import { Router } from "@angular/router";

import { AF } from "./providers/af";
import {SharedService} from "./providers/shared-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public afService: AF, private router: Router, private _sharedService: SharedService) {
    this.router.navigate(['splash']);
    this.afService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {
          this.router.navigate(['login']);
        }
        else {
          if(auth.google) {
            this.afService.displayName = auth.google.displayName;
            this.afService.email = auth.google.email;
            this.afService.uid = auth.uid;
            this.afService.photoURL = auth.google.photoURL;
            this._sharedService.setDisplayName(auth.google.displayName);
            this._sharedService.setPhotoUrl(auth.google.photoURL);
          }
          else {
            this.afService.displayName = auth.auth.displayName;
            this.afService.email = auth.auth.email;
            this.afService.uid = auth.auth.uid;
            this.afService.photoURL = auth.auth.photoURL;
            this._sharedService.setDisplayName(auth.auth.displayName);
            this._sharedService.setPhotoUrl(auth.auth.photoURL);
          }
          this._sharedService.setisLoggedIn(true);
          this._sharedService.emitUserDataChange(true);
          this.router.navigate(['']);
        }
      }
    );
  }

  logout() {
    this.afService.logout();
  }
}
